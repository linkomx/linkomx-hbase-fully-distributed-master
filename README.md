# Linko MX - HBase Fully Distributed Master

Docker image of the HBase in Fully Distributed mode for master.

## Requirements

  - Docker 20

## Contributing

### Git config

```bash
$ git config --local "user.name" "myusernameatgitlab"
```

```bash
$ git config --local "user.email" "myemail@linko.mx"
```

## Building

### Local installation

For local installation use:

```bash
$ docker run \
    -u $(id -u):$(grep -w docker /etc/group | awk -F\: '{print $3}') \
    --rm \
    -w $(pwd) \
    -v /etc/group:/etc/group:ro \
    -v /etc/passwd:/etc/passwd:ro \
    -v $(pwd):$(pwd) \
    -v ${HOME}/.m2:${HOME}/.m2 \
    -v /var/run/docker.sock:/var/run/docker.sock \
    azul/zulu-openjdk-alpine:8u282 \
    ./mvnw -Djansi.force=true -ntp -U clean package
```

Then launch the with:

```bash
$ docker network create hbase
```

```bash
$ for i in {1..3}; \
    do \
      docker run -d \
        --network=hbase \
        --name=hadoop-slave-$i \
        --hostname=hadoop-slave-$i \
        registry.gitlab.com/linkomx/linkomx-hadoop:2.10.1; \
  done
```

```bash
$ docker run -d \
    --network=hbase \
    --name=hadoop-master \
    --hostname=hadoop-master \
    registry.gitlab.com/linkomx/linkomx-hadoop-master:2.10.1
```

```bash
$ for i in {1..3}; do \
    docker run -d \
      --network=hbase \
      --name=zk-$i \
      --hostname=zk-$i \
      -e ZOO_MY_ID=$i \
      -e ZOO_ADMINSERVER_ENABLED=false \
      -e ZOO_SERVERS="server.1=zk-1:2888:3888;2181 server.2=zk-2:2888:3888;2181 server.3=zk-3:2888:3888;2181" \
      zookeeper:3.5.6; \
  done
```

```bash
$ for i in {1..3}; \
    do \
      docker run -d \
        --network=hbase \
        --name=hbase-node-$i \
        --hostname=hbase-node-$i \
        registry.gitlab.com/linkomx/linkomx-hbase-fully-distributed:2.2.7; \
  done
```

```bash
$ docker run -d \
    --network=hbase \
    --name=hbase-master \
    --hostname=hbase-master \
    linkomx-hbase-fully-distributed-master:2.2.7
```